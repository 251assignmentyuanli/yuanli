package nz.ac.massey.cs;

import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RadioChoice;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.resource.ResourceReference;

import net.sf.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TasksPage extends WebPage {

	private static final long serialVersionUID = 1L;
	int num=0;

	public TasksPage() throws ClassNotFoundException, SQLException, ParseErrorException, MethodInvocationException, ResourceNotFoundException, IOException {
		
        add(new EntryForm("entryForm"));

        Form listForm = new Form("listForm");

        add(listForm);

		Date now = new Date();
		Label dateTimeLabel = new Label("datetime", now.toString());
		add(dateTimeLabel);
		//show the running environment.
		Label run = new Label("envir","Run environment:"+System.getProperty("wicket.configuration"));
		add(run);
		PartList list=PartList.getInstance();//this is a class which is singleton pattern
		WicketApplication app = (WicketApplication) this.getApplication();
		TaskList collection = app.getTaskList();
		List<Task> tasks = collection.getTasks();
		
		
		PropertyListView taskListView =
				new PropertyListView("task_list", tasks) {
					private static final long serialVersionUID = 1L;

					@Override
					protected void populateItem(ListItem item) {

						item.add(new Label("name"));
						item.add(new Label("description"));
						 item.add(new Label("date"));
	                        item.add(new Label("project"));

						item.add(new AjaxCheckBox("completed") {
							@Override
							protected void onUpdate(AjaxRequestTarget ajaxRequestTarget) {
							}
						});
					}
				};

		add(new Link<Void>("selectAll") {
			@Override
			public void onClick() {
				for(Task t: tasks) {
					t.setComplete(true);
				}
				//make sure all tasks are selected
				for(Task t:list.getremberList()) {
					if(t.isComplete()==false) {
						t.setComplete(true);
					}
				}

			}
		});

	//clear tasks which had been completed
		add(new Link<Void>("c") {
			@SuppressWarnings("null")
			@Override
			public void onClick() {
				List<Task> clearSave = new ArrayList<Task>();
				for (Task t:tasks) {
					if (t.isComplete()) {
						clearSave.add(t);
					}
					
				}
				int count=0;
				for(Task j:list.getremberList()) {
					if (j.isComplete()) {
						count=count+1;
					}
				}
				if (count !=0) {
					list.clearRem();
				}
				//save the tasks cleared in clearSave;
				for (Task i:clearSave) {
					collection.removeTask(i);
					list.addClear(i);
				}
			}
		});
		
	//show all tasks
		add(new Link<Void>("all") {
			@Override
			public void onClick() {
				for(Task t:list.getremberList()) {
					collection.addTask(t);
				}
				list.clearRem();
			}
		});

		//show active tasks ,move completed tasks to remeberlist
		add(new Link<Void>("act") {
			@Override
			
			public void onClick() {
				//check if remeberlist have active tasks
				for(Task t:list.getremberList()) {
					if(t.isComplete()==false) {
					collection.addTask(t);}
				}
				list.clearRem();
				List<Task> l = new ArrayList<Task>();
				for (Task t:tasks) {
					if (t.isComplete()) {
					l.add(t);
					}
					
				}
				
				for (Task i:l) {
					collection.removeTask(i);
					list.addRem(i);
				}
				
			}
		});
		//move the active tasks to rememberlist.show completed tasks
		add(new Link<Void>("com") {
			@Override
			public void onClick() {
				for(Task t:list.getremberList()) {
					if(t.isComplete()) {
					collection.addTask(t);}
				}
				list.clearRem();
				List<Task> l = new ArrayList<Task>();
				for (Task t:tasks) {
					if (t.isComplete()==false) {
					l.add(t);
					}
					
				}
				for (Task i:l) {
					collection.removeTask(i);
					list.addRem(i);
				}
			
				

			}
		});
		
		

	
		//check database and update state;	
		SQLliteJDBC sql=SQLliteJDBC.getInstance();
		sql.getConnect();
		sql.checkDate(list.getclearList());
		
		sql.checkDate(list.getremberList());
		sql.checkDate(tasks);
		sql.releaseConnection();

		

		//count the number of incompleted tasks;
		for(Task t: list.getremberList()) {
			if(t.isComplete()==false) {
				num=num+1;
			}
			
		}
				for(Task t: tasks) {
					if(t.isComplete()==false) {
						num=num+1;
					}
					
				}
				
				
				//a link jump to a new taskpage to update the page;
				 PageParameters parameters = new PageParameters();
	              parameters.add("countIn", num);
	             add(new BookmarkablePageLink<Object>("bookmarkablePageLink",TasksPage.class,parameters) );
	             //the label show the incompleted tasks'number
	             add(new Label("countNum",num));

	             
	           
	            
	             
		   listForm.add(taskListView);

				}
}
