                                           Readme
1.OUR MEMBERS:
Baiyu Yuan ID:17140736
Rao Li     ID:17139967

2.how to run my program?
    First step:
    We have a shell scripting file,if you run the shell.sh file,the error file will be moved to the errors dir;And the no-duedate todos will moved to the no-duedate dir;And the no error todos will be moved to the noError.md file.then the first step is over till now.
    
    Second step:
    You just need to run the start file, and we creat a new database(name:data) which is linked to the webapp.Then you click the html file,you can add things to the form,and click add,the data will be saved in the database as a line of the table.It has ID,projectname,and duedate and so on attributes.And it also can show whether the todo is completed or not when you click the button"Update your data"to update your database.
    We have filter fonction,every time you click different buttons it will show all,active and completed state todos.
    At the same time,if you run the start file,and the todos we saved in the noErrors file can be added to the database.

    The new dir and files:
    The noError file to save the no error todos.
    The Shell.sh is the code of the shell scripting.
3.Our team name:251-Assignment-YuanLi
Team Id:251assignmentyuanli 
Our team member:Berry,karroy,Teacher Li Sui,Teacher Peter.
In the bitbucket we created.
karroy is Rao Li,
and Berry is Baiyu Yuan.
You can see what we do in the submition of the files.


Author	Submition	                                  
karroy  ae78099         
Berry   47b1ba6          
karroy  d7cac0e
Berry   35e8052
karroy  1a6138b
Berry   d03ca26
Berry   f1b4c2e
karroy  936fbc5
Berry   9cd5f94


4.Metrics analysis(name is metrics report in the webapp dir): 

I.Cyclomatic Complexity: <Metric id="VG" description="McCabe Cyclomatic Complexity" max="10" hint="use Extract-method to split the method up">
<Values per="method" avg="1.59" stddev="1.891" max="12" maxinrange="false">...</Values>
</Metric>
II. Number of Methods (NOM):
<Metric id="NOM" description="Number of Methods">
<Values per="type" total="36" avg="3.6" stddev="2.905" max="9">
III. Lines of Code (LOC):
<Metric id="TLOC" description="Total Lines of Code">
<Value value="605"/>
</Metric>
IV.Coupling Between Objects (CBO):
<Metric id="CA" description="Afferent Coupling">
<Values per="packageFragment" avg="0" stddev="0" max="0">...</Values>
</Metric>
<Metric id="CE" description="Efferent Coupling">
<Values per="packageFragment" avg="3.5" stddev="1.5" max="5">...</Values>
</Metric>
V.God Classes
<Metric id="LCOM" description="Lack of Cohesion of Methods">
<Values per="type" avg="0.189" stddev="0.301" max="0.854">
<Value name="Task" source="Task.java" package="nz.ac.massey.cs" value="0.854"/>
<Value name="SQLliteJDBC" source="SQLliteJDBC.java" package="nz.ac.massey.cs" value="0.533"/>
<Value name="PartList" source="PartList.java" package="nz.ac.massey.cs" value="0.5"/>
<Value name="EntryForm" source="EntryForm.java" package="nz.ac.massey.cs" value="0"/>
<Value name="ReadFile" source="ReadFile.java" package="nz.ac.massey.cs" value="0"/>
<Value name="TaskList" source="TaskList.java" package="nz.ac.massey.cs" value="0"/>
<Value name="TasksPage" source="TasksPage.java" package="nz.ac.massey.cs" value="0"/>
<Value name="TasksPage.anonymous#[" source="TasksPage.java" package="nz.ac.massey.cs" value="0"/>
<Value name="TasksPage.anonymous#[!2" source="TasksPage.java" package="nz.ac.massey.cs" value="0"/>
<Value name="TasksPage.anonymous#[!3" source="TasksPage.java" package="nz.ac.massey.cs" value="0"/>
<Value name="TasksPage.anonymous#[!4" source="TasksPage.java" package="nz.ac.massey.cs" value="0"/>
<Value name="TasksPage.anonymous#[!5" source="TasksPage.java" package="nz.ac.massey.cs" value="0"/>
<Value name="TasksPage.anonymous#[!6" source="TasksPage.java" package="nz.ac.massey.cs" value="0"/>
<Value name="TasksPage.anonymous#[.anonymous#" source="TasksPage.java" package="nz.ac.massey.cs" value="0"/>
<Value name="WicketApplication" source="WicketApplication.java" package="nz.ac.massey.cs" value="0"/>
<Value name="Start" source="Start.java" package="nz.ac.massey.cs" value="0"/>
<Value name="TestHomePage" source="TestHomePage.java" package="nz.ac.massey.cs" value="0"/>
</Values>
</Metric>

  
 PMD analysis(for example,whole file name is pmd-report.txt in the webapp dir):

src/main/java/nz/ac/massey/cs/EntryForm.java:3:	Avoid unused imports such as 'java.io.IOException'
src/main/java/nz/ac/massey/cs/EntryForm.java:4:	Avoid unused imports such as 'java.sql.Connection'
src/main/java/nz/ac/massey/cs/EntryForm.java:5:	Avoid unused imports such as 'java.sql.SQLException'
src/main/java/nz/ac/massey/cs/EntryForm.java:8:	Avoid unused imports such as 'org.apache.velocity.exception.MethodInvocationException'
src/main/java/nz/ac/massey/cs/EntryForm.java:9:	Avoid unused imports such as 'org.apache.velocity.exception.ParseErrorException'
src/main/java/nz/ac/massey/cs/EntryForm.java:10:	Avoid unused imports such as 'org.apache.velocity.exception.ResourceNotFoundException'
src/main/java/nz/ac/massey/cs/EntryForm.java:11:	Avoid unused imports such as 'org.apache.wicket.MarkupContainer'
src/main/java/nz/ac/massey/cs/EntryForm.java:14:	Avoid unused imports such as 'org.apache.wicket.markup.html.form.TextField'
src/main/java/nz/ac/massey/cs/EntryForm.java:15:	Avoid unused imports such as 'org.apache.wicket.model.IModel'
src/main/java/nz/ac/massey/cs/EntryForm.java:20:	Header comments are required
src/main/java/nz/ac/massey/cs/EntryForm.java:22:	Field comments are required
src/main/java/nz/ac/massey/cs/EntryForm.java:22:	Found non-transient, non-static member. Please mark as transient or provide accessors.
src/main/java/nz/ac/massey/cs/EntryForm.java:22:	Private field 'nameField' could be made final; it is only initialized in the declaration or constructor.


5.We have bitbucket account,and every time if we change our files,we'll update the new files we improved.Also,we have set a few questions,and we communicate them.