package nz.ac.massey.cs;


import java.io.IOException;
import java.io.StringWriter;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

public class SQLliteJDBC{

	  Connection c = null;  
	  Statement stmt = null;
	  List<String> l=new LinkedList();
	 
	public void setL(List<String> l) {
		this.l = l;
	}
		  
			private static SQLliteJDBC instance=new SQLliteJDBC();
		    private SQLliteJDBC(){
		        
		    }
		    public static SQLliteJDBC getInstance(){

                return instance;
		    }
//build table head,the table name is project title;		
		    //use the velocity engine to add variable in sqlite language
  public void getState(String table) throws SQLException, ParseErrorException, MethodInvocationException, ResourceNotFoundException, IOException {
	 
	  stmt = c.createStatement();
	  VelocityEngine engine = new VelocityEngine();
		VelocityContext context = new VelocityContext();
		context.put("table", table);
		StringWriter writer = new StringWriter();
      String sql = "CREATE TABLE ${table} " +   
    		       "(ID        INT     NOT NULL," +
                   " Name      TEXT    NOT NULL, " + 
                   " Description    TEXT    NOT NULL, " + 
                   " Date        TEXT      NOT NULL, " + 
                   " State       BOOLEAN       NOT NULL)"; 
      engine.evaluate(context, writer, "",sql );
      stmt.executeUpdate(writer.toString());
      stmt.close();
	 
  }
  
  
  
  //get connection with database;
  public void getConnect() throws ClassNotFoundException, SQLException {
	 
	  Class.forName("org.sqlite.JDBC");
      c = DriverManager.getConnection("jdbc:sqlite:data.db");
      
  }
  
  //release connection with database
  public void releaseConnection() {
      try {
           if (c != null)
                c.close();
          } catch (Exception e) {
            e.printStackTrace();
          }
      }
 
  //save data
  public void addDate(Task t) throws SQLException, ParseErrorException, MethodInvocationException, ResourceNotFoundException, IOException {
	 
	  if(l.contains(t.getProject())==false){
		  SQLliteJDBC sql=SQLliteJDBC.getInstance();
		  
		  sql.getState(t.getProject());
		  l.add(t.getProject());
	  }
	 
	  VelocityEngine engine = new VelocityEngine();
		VelocityContext context = new VelocityContext();
		context.put("id",t.getId() );
		context.put("n", t.getName());
		context.put("d", t.getDescription());
		context.put("da", t.getDate());
		context.put("p", t.getProject());
		context.put("s", t.isComplete());
		StringWriter writer = new StringWriter();
		
	  String sql = "INSERT INTO ${p} (ID,Name,Description,Date,State)"
			  +"VALUES(${id},'${n}','${d}','${da}','${s}');";
	  engine.evaluate(context, writer, "",sql );
	  stmt = c.createStatement();
	  stmt.executeUpdate(writer.toString());
	 stmt.close();
     
  }
  
  //check database and update state.
  public void checkDate(List<Task> list) throws SQLException, ParseErrorException, MethodInvocationException, ResourceNotFoundException, IOException {
	  VelocityEngine engine = new VelocityEngine();
		VelocityContext context = new VelocityContext();
		
	  for(Task t:list) {
		  context.put("s", t.isComplete());
			context.put("id",t.getId() );
			context.put("p", t.getProject());
			StringWriter writer = new StringWriter();
			 String sql = "UPDATE ${p} set State = '${s}' where ID=${id}";
			 
			  engine.evaluate(context, writer, "",sql );
			  stmt = c.createStatement();
			  stmt.executeUpdate(writer.toString());
			  stmt.close();
 
  }
	 
	 
  }
  
  }

