package nz.ac.massey.cs;

import java.io.Serializable;
import java.util.Date;

import org.apache.wicket.util.file.File;

// This class models a Task item

public class Task implements Serializable {

    private String description;
    private boolean completed;
    private String name;
    private Integer id;
    private String date;
    private String project;


    public Task(String name,String description,String date,String project) {
    	this.date= date;
    	this.project=project;//the title
        this.name = name;
        this.description = description;
        this.completed = false;//the tasks'statement
    }
  
    public boolean isComplete() {
        return completed;
    }
    public void setComplete(boolean complete) { completed = complete; }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}

	public String getDate() {
		return date;
	}

	public String getProject() {
		return project;
	}
    
    
    
}


