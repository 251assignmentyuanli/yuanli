package nz.ac.massey.cs;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class PartList implements Serializable{
	//We use clearList to save tasks which are cleared ,
	//we use remember list to save the tasks we don't want to show that for the moment
    private List<Task> clearList=new LinkedList<Task>();
    private List<Task> remember= new LinkedList<Task>();
    
    
    
        private static PartList instance=new PartList();
        private PartList(){
            
        }
        public static PartList getInstance(){
            return instance;
        }
    
    
   

    public List<Task> getclearList() {
        return clearList;
    }
    public List<Task> getremberList() {
        return remember;
    }
    
    public void addClear(Task task) {
    	clearList.add(task);
    }
    public void addRem(Task task) {
    	remember.add(task);
    }
   
    
    public void clearRem() {
    	remember.clear();
    }
   public int getLength() {
	   return clearList.size()+remember.size();
   }
}
