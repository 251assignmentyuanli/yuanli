package nz.ac.massey.cs;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

// form with two fields for adding a task item

public class EntryForm extends Form<Void> {

    private RequiredTextField nameField;
    private RequiredTextField descriptionField;
    private RequiredTextField dateField;
    private RequiredTextField projectField;
    
    public EntryForm(String id) {
        super(id);
        nameField = new RequiredTextField("name", Model.of(""));
        descriptionField = new RequiredTextField("description", Model.of(""));
        dateField = new RequiredTextField("date", Model.of(""));
        projectField = new RequiredTextField("project", Model.of(""));
        
        add(nameField);
        add(descriptionField);
        add(dateField);
        add(projectField);
    
        
    }

    // adds the task when the form is submitted (by clicking the Add button)
    protected void onSubmit() {
        super.onSubmit();
        String name = (String)nameField.getDefaultModelObject();
        String description = (String)descriptionField.getDefaultModelObject();
        String date = (String)dateField.getDefaultModelObject();
        String project = (String)projectField.getDefaultModelObject();
        
        descriptionField.clearInput();
        descriptionField.setModelObject(null);
        nameField.clearInput();
        nameField.setModelObject(null);
        dateField.clearInput();
        dateField.setModelObject(null);
        projectField.clearInput();
        projectField.setModelObject(null);

        WicketApplication app = (WicketApplication) this.getApplication();
        TaskList collection = app.getTaskList();
        List<Task> tasks = collection.getTasks();
        Task task=new Task(name,description,date,project);
        task.setId(PartList.getInstance().getLength()+tasks.size()+9);
        collection.addTask(task);
        //at the same time ,add the data to database
        SQLliteJDBC sql=SQLliteJDBC.getInstance();
      
        try {
        	
        	sql.getConnect();
			sql.addDate(task);
		sql.releaseConnection();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
       
        
	
    }
}