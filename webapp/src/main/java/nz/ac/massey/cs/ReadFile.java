package nz.ac.massey.cs;

import java.io.*;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;


public class ReadFile {

	//a method which add data whic in data file 
	public void back() throws ClassNotFoundException, SQLException {
	
		  File file = new File("data\\files\\noError.md");
		
		  
		  
		  
		  SQLliteJDBC sql=SQLliteJDBC.getInstance();
    
      sql.setL(new LinkedList());
			sql.getConnect();
		
		  BufferedReader reader = null;
	        try {
	        	
	            reader = new BufferedReader(new FileReader(file));
	            String tempString = null;
	            String project=null;
	            int id=0;
	            while ((tempString = reader.readLine()) != null) {
	               
	            	
	            	if(tempString.contains("#") & tempString.contains("##")==false) {
	                project=tempString.substring(2);
	                
	                }
	            	else if(tempString.contains("#")==false & tempString.length()>3) {
	            		Boolean state=true;
	            		String date="no duedate";
	            		String name=null;
	            		String description="no description";
	            		if(tempString.substring(0, 3).equals("[X]")) {
	            			state=false;
	            			
	            		}
	            		
	            		if(tempString.contains("due")) {
	            		String[] l =tempString.split("due:");
	            		date=l[1];
	            		if(tempString.contains("(A)")) {
		            		description="A";
		            		String[] l2=l[0].split("(A)");
		            		name=l2[1].substring(1);
		            		
	            		}
	            		else {
	            			name=l[0].substring(3);
	            		}
	            		}
	            		else if(tempString.contains("due")==false) {
		            		if(tempString.contains("(A)")) {
		            		description="A";
		            		String[] l2=tempString.split("(A)");
		            		name=l2[1].substring(1);
		            		}
		            		else {
		            			name=tempString.substring(3);
		            		}
	            		}
	            		
	            		id++;
	            	Task t=new Task(name,description,date,project);
	            	t.setComplete(state);
	            	t.setId(id);
//	            	System.out.println(t.getName()+t.getDescription()+t.getDate()+t.getProject()+t.isComplete());
	            	sql.getConnect();
	    				sql.addDate(t);
	    				sql.releaseConnection();
	    	
	            	}
	            	
	            	
	            }
	            
	            reader.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (reader != null) {
	                try {
	                    reader.close();
	                } catch (IOException e1) {
	                }
	            }
	        }
	    }
	        
	        
	        
	    


	}


